#!/bin/bash
read -p "The steps to create the intermediate cert directories is the same as the root cert process"
mkdir -p ca/intermediate ca/intermediate/certs ca/intermediate/crl ca/intermediate/csr ca/intermediate/newcerts ca/intermediate/private
chmod 700 ca/intermediate/private
touch ca/intermediate/index.txt
echo 1000 > ca/intermediate/serial
# crlnumber is for tracking certiicate revocation lists
echo 1000 > ca/intermediate/crl.cnf
