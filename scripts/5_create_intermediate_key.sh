#!/bin/bash
cd ca
read -p "Generate the intermediate private key and set permissions.
openssl genrsa -aes256 \
      -out intermediate/private/intermediate.key.pem 4096"
openssl genrsa -aes256 \
      -out intermediate/private/intermediate.key.pem 4096
chmod 400 intermediate/private/intermediate.key.pem
