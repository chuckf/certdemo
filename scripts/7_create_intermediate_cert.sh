#!/bin/bash
cd ca
read -p "Sign the Intermediate CSR with the Root CA to generate the Intermediate Certificate
openssl ca -config openssl.cnf -extensions v3_intermediate_ca \
      -days 3650 -notext -md sha256 \
      -in intermediate/csr/intermediate.csr.pem \
      -out intermediate/certs/intermediate.cert.pem"
openssl ca -config openssl.cnf -extensions v3_intermediate_ca \
      -days 3650 -notext -md sha256 \
      -in intermediate/csr/intermediate.csr.pem \
      -out intermediate/certs/intermediate.cert.pem
echo ""
read -p "Set Permissions
chmod 444 intermediate/certs/intermediate.cert.pem"

echo ""
chmod 444 intermediate/certs/intermediate.cert.pem
echo ""
echo ""
echo "Show index.txt, where the ca tool stores the cert database"
read -p "Do not edit this by hand"
cat intermediate/index.txt
echo ""
read -p "Verify the intermediate cert
openssl x509 -noout -text \
      -in intermediate/certs/intermediate.cert.pem"
openssl x509 -noout -text \
      -in intermediate/certs/intermediate.cert.pem
echo ""
echo "Verify the intermediate against the root cert
openssl verify -CAfile certs/ca.cert.pem \
      intermediate/certs/intermediate.cert.pem"
read -p "If it all went well, we'll see an OK"
openssl verify -CAfile certs/ca.cert.pem \
      intermediate/certs/intermediate.cert.pem

