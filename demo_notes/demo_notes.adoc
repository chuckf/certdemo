= Demo Notes

== Create Root Pair

. create and change to temp dir
. Show and run `1_prepare_directory.sh`
. copy cnf file `cp files/ca/openssl.cnf` `ca/`
. Run `2_create_root_key` for ca.key.pem
. Run `3_create_root_cert` for creating and verifying the root certificate

== Create Intermediate Pair

. Run `4_prepare_intermediate_dir.sh`
. copy cnf file `cp files/intermediate/openssl.cnf` `ca/intermediate/`
. run `5_create_intermediate_key.sh`
. run `6_create_intermediate_csr.sh`
. run `7_create_intermediate_cert.sh`
. run  `8_create_cert_chain.sh`

== Create, sign, and deploy server certs

. run `9_create_server_key.sh`
. run `10_deploy_cert.sh`

== Look for the CHANGEMEs